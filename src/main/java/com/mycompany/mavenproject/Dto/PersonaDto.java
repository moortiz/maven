/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject.Dto;

import java.util.Date;

/**
 *
 * @author SUBOCOL
 */
public class PersonaDto {
    int id;
    String nombre;
    String apellido;
    String numeroDocumento;
    Date fechaNacimento;
    TipoDocumetoDto tipoDocumento;

    public PersonaDto() {
    }

    public PersonaDto(int id, String nombre, String apellido, String numeroDocumento, Date fechaNacimento, TipoDocumetoDto tipoDocumento) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numeroDocumento = numeroDocumento;
        this.fechaNacimento = fechaNacimento;
        this.tipoDocumento = tipoDocumento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Date getFechaNacimento() {
        return fechaNacimento;
    }

    public void setFechaNacimento(Date fechaNacimento) {
        this.fechaNacimento = fechaNacimento;
    }

    public TipoDocumetoDto getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumetoDto tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    
}
