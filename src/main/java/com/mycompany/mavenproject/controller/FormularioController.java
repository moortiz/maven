/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject.controller;

import com.mycompany.mavenproject.Dto.PersonaDto;
import com.mycompany.mavenproject.Dto.TipoDocumetoDto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;


/**
 *
 * @author SUBOCOL
 */
@Named(value = "formularioController")
@ViewScoped
public class FormularioController implements Serializable{

    PersonaDto personaDto=new PersonaDto();
    int id=0;
    List<TipoDocumetoDto> documeto;
    List<PersonaDto> personaDtos = new ArrayList<>();
    
    @PostConstruct
    public void init() {
        documeto = new ArrayList<>();

        documeto.add(new TipoDocumetoDto(1,"Cedula"));
        documeto.add(new TipoDocumetoDto(2,"Tarjeta identidad"));

    }
    
    public FormularioController() {
    }

    public void crearPersona(){
        
        personaDtos.add(new PersonaDto(personaDtos.size()+1, personaDto.getNombre(), personaDto.getApellido(), 
                personaDto.getNumeroDocumento(), personaDto.getFechaNacimento(), documeto.get(id-1)));
        
    }

    public void deletePersona() {
        this.personaDtos.remove(this.personaDto);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Removed"));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-products");
    }
    
    public void savePersona() {
        if (this.personaDto.getId() == 0) {
            this.personaDto.setId(personaDtos.size()+1);
            this.personaDtos.add(this.personaDto);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Added"));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Updated"));
        }

        PrimeFaces.current().executeScript("PF('manageProductDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-products");
    }

    public List<PersonaDto> getPersonaDtos() {
        return personaDtos;
    }

    public void setPersonaDtos(List<PersonaDto> personaDtos) {
        this.personaDtos = personaDtos;
    }
    
    public PersonaDto getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDto personaDto) {
        this.personaDto = personaDto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void TipoDocumentoController() {
    }
 
    public List<TipoDocumetoDto> getDocumetoDto() {
        return documeto;
    }

    public void setDocumetoDto(List<TipoDocumetoDto> documeto) {
        this.documeto = documeto;
    }
    
    
    
}
